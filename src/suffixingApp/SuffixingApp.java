import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class SuffixingApp {

    private static final Logger LOGGER = Logger.getLogger(SuffixingApp.class.getName());

    static {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.INFO);

        SimpleFormatter formatter = new SimpleFormatter();
        consoleHandler.setFormatter(formatter);

        LOGGER.addHandler(consoleHandler);
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            LOGGER.log(Level.SEVERE, "Usage: java SuffixingApp <configFilePath>");
            return;
        }

        String configFilePath = args[0];
        Properties config = loadConfig(configFilePath);

        if (config != null) {
            processFiles(config);
        }
    }

    private static Properties loadConfig(String configFilePath) {
        Properties config = new Properties();

        try (FileInputStream input = new FileInputStream(configFilePath)) {
            config.load(input);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error loading config file: " + configFilePath, e);
            return null;
        }

        return config;
    }

    private static void processFiles(Properties config) {
        String mode = config.getProperty("mode");
        String suffix = config.getProperty("suffix");
        String filesList = config.getProperty("files");

        if (!"copy".equalsIgnoreCase(mode) && !"move".equalsIgnoreCase(mode)) {
            LOGGER.log(Level.SEVERE, "Mode is not recognized: " + mode);
            return;
        }

        if (suffix == null) {
            LOGGER.log(Level.SEVERE, "No suffix is configured");
            return;
        }

        if (filesList == null || filesList.isEmpty()) {
            LOGGER.log(Level.WARNING, "No files are configured to be copied/moved");
            return;
        }

        for (String filePath : filesList.split(":")) {
            processFile(filePath, mode, suffix);
        }
    }

    private static void processFile(String filePath, String mode, String suffix) {
        Path sourcePath = Paths.get(filePath.replace("/", File.separator));
        String fileName = sourcePath.getFileName().toString();
        String fileExtension = fileName.contains(".") ? fileName.substring(fileName.lastIndexOf(".")) : "";
        String destinationFileName = fileName.replace(fileExtension, "") + suffix + fileExtension;
        Path destinationPath = sourcePath.resolveSibling(destinationFileName);

        try {
            if ("copy".equalsIgnoreCase(mode)) {
                Files.copy(sourcePath, destinationPath);
                LOGGER.log(Level.INFO, sourcePath + " -> " + destinationPath);
            } else if ("move".equalsIgnoreCase(mode)) {
                Files.move(sourcePath, destinationPath);
                LOGGER.log(Level.INFO, sourcePath + " => " + destinationPath);
            }
        } catch (FileAlreadyExistsException e) {
            LOGGER.log(Level.SEVERE, "File already exists: " + destinationPath);
        } catch (NoSuchFileException e) {
            LOGGER.log(Level.SEVERE, "No such file: " + sourcePath);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error processing file: " + sourcePath, e);
        }
    }
}
